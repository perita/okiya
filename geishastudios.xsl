<?xml version='1.0'?>
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 <xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/docbook.xsl"/>
 <xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/highlight.xsl"/>
 <xsl:output method="xml" encoding="UTF-8" indent="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" xmlns="http://www.w3.org/1999/xhtml"/>
 <xsl:param name="generate.id.attributes" value="1"/>
 <xsl:param name="link.mailto.url">mailto:jfita@geishastudios.com</xsl:param>
 <xsl:param name="site.topdir">..</xsl:param>
 <xsl:param name="highlight.source" select="1"/>

 <!-- DocBooks adds the title attribute to the article's div, but this
 results in some browsers to show up the article's title in a tooltip.
 I find this rather obnoxious and this here I remove all title attributes. -->
 <xsl:template name="generate.html.title" />

 <!-- Remove 'Abstract' title -->
 <xsl:template match="abstract" mode="title.markup" />

 <!-- Top level's title is already h2, so I have to shift the headings one. -->
 <xsl:template name="section.heading">
  <xsl:param name="section" select="."/>
  <xsl:param name="level" select="1"/>
  <xsl:param name="allow-anchors" select="1"/>
  <xsl:param name="title"/>
  <xsl:param name="class" select="'title'"/>

  <xsl:variable name="id">
   <xsl:choose>
    <!-- Make sure the subtitle doesn't get the same id as the title -->
    <xsl:when test="self::subtitle">
     <xsl:call-template name="object.id">
      <xsl:with-param name="object" select="."/>
     </xsl:call-template>
    </xsl:when>
    <!-- if title is in an *info wrapper, get the grandparent -->
    <xsl:when test="contains(local-name(..), 'info')">
     <xsl:call-template name="object.id">
      <xsl:with-param name="object" select="../.."/>
     </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
     <xsl:call-template name="object.id">
      <xsl:with-param name="object" select=".."/>
     </xsl:call-template>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>

  <!-- HTML H level is one higher than section level -->
  <xsl:variable name="hlevel">
   <xsl:choose>
    <!-- highest valid HTML H level is H6; so anything nested deeper
    than 4 levels down just becomes H6 -->
    <xsl:when test="$level &gt; 4">6</xsl:when>
    <xsl:otherwise>
     <xsl:value-of select="$level + 2"/>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  <xsl:element name="h{$hlevel}" namespace="http://www.w3.org/1999/xhtml">
   <xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>
   <xsl:if test="$css.decoration != '0'">
    <xsl:if test="$hlevel&lt;3">
     <xsl:attribute name="style">clear: both</xsl:attribute>
    </xsl:if>
   </xsl:if>
   <xsl:if test="$allow-anchors != 0 and $generate.id.attributes = 0">
    <xsl:call-template name="anchor">
     <xsl:with-param name="node" select="$section"/>
     <xsl:with-param name="conditional" select="0"/>
    </xsl:call-template>
   </xsl:if>
   <xsl:if test="$generate.id.attributes != 0 and not(local-name(.) = 'appendix')">
    <xsl:attribute name="id"><xsl:value-of select="$id"/></xsl:attribute>
   </xsl:if>
   <xsl:copy-of select="$title"/>
  </xsl:element>
 </xsl:template>

 <!-- Table of contents inserted by <?asciidoc-toc?> processing instruction. -->
 <xsl:param name="generate.toc">
  <xsl:choose>
   <xsl:when test="/processing-instruction('asciidoc-toc')">
    article nop
    book    toc,title,figure,table,example,equation
   </xsl:when>
   <xsl:otherwise>
    article nop
    book    nop
   </xsl:otherwise>
  </xsl:choose>
 </xsl:param>

 <!-- Numbered sections inserted by <?asciidoc-numbered?> processing instruction. -->
 <xsl:param name="section.autolabel">
  <xsl:choose>
   <xsl:when test="/processing-instruction('asciidoc-numbered')">1</xsl:when>
   <xsl:otherwise>0</xsl:otherwise>
  </xsl:choose>
 </xsl:param>

 <!-- comments inserted by <?asciidoc-commnets?> processing instruction. -->
 <xsl:param name="page.comments">
  <xsl:choose>
   <xsl:when test="/processing-instruction('asciidoc-comments')">1</xsl:when>
   <xsl:otherwise>0</xsl:otherwise>
  </xsl:choose>
 </xsl:param>

 <!-- sidebar inserted by <?asciidoc-sidebar?> processing instruction. -->
 <xsl:param name="page.sidebar">
  <xsl:choose>
   <xsl:when test="/processing-instruction('asciidoc-sidebar')">1</xsl:when>
   <xsl:otherwise>0</xsl:otherwise>
  </xsl:choose>
 </xsl:param>

 <!-- Custom class values for article -->
 <xsl:template match="article" mode="class.value">
  <xsl:choose>
   <xsl:when test="$page.sidebar != 0"><xsl:value-of select="'article sidebar'"/></xsl:when>
   <xsl:otherwise><xsl:value-of select="'article'"/></xsl:otherwise>
  </xsl:choose>
 </xsl:template>

 <!-- Additional class values for sections -->
 <xsl:template match="*[@role]" mode="class.value">
  <xsl:value-of select="name()"/><xsl:text> </xsl:text><xsl:value-of select="@role"/>
 </xsl:template>

 <!-- Add text to the document's title tag. -->
 <xsl:template match="article" mode="object.title.markup.textonly">
  <xsl:variable name="title">
   <xsl:apply-templates select="." mode="object.title.markup"/>
  </xsl:variable>
  <xsl:value-of select="normalize-space($title)"/><xsl:text> - Geisha Studios</xsl:text>
 </xsl:template>

 <!-- Contents in <head> -->
 <xsl:template name="user.head.content">
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="{$site.topdir}/screen.css" type="text/css" />
 </xsl:template>

 <!-- Header inside of <body> -->
 <xsl:template name="user.header.content">
  <div class="header">
   <div id="nav">
    <h1 id="logo"><a href="http://www.geishastudios.com/" rel="home">Geisha Studios</a></h1>
    <ul>
     <li><a href="{$site.topdir}/games/index.html">Games</a></li>
     <li><a href="{$site.topdir}/literate/README.html">Literate Programming</a></li>
     <li><a href="{$site.topdir}/about.html">About</a></li>
     <li><a href="{$site.topdir}/contact.html">Contact</a></li>
    </ul>
   </div>
  </div>
 </xsl:template>

 <!-- Footer -->
 <xsl:template name="user.footer.content">
  <xsl:param name="title">
   <xsl:apply-templates select="/" mode="object.title.markup.textonly"/>
  </xsl:param>
  <xsl:if test="$page.comments != 0">
   <div class="comments">
    <h3 class="title" style="clear: both"><a id="comments"></a>Comments</h3>
    <div id="disqus_thread"></div>
    <script type="text/javascript">
     var disqus_identifier = '<xsl:value-of select="$title"/>';
     //<![CDATA[
     (function() {
     var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
     dsq.src = 'http://geishastudios.disqus.com/embed.js';
     (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
     })();
     //]]>
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript=geishastudios">comments powered by Disqus.</a></noscript>
   </div>
  </xsl:if>
  <div id="addThis">
   <p><a class="addthis_button" href="http://addthis.com/bookmark.php?v=250&amp;pub=xa-4b0bb5610767eb0e"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pub=xa-4b0bb5610767eb0e"></script></p>
  </div>
  <div id="copyright">
   <p>© 2009, 2010, 2011, 2012 Geisha Studios</p>
  </div>
  <xsl:if test="$page.comments != 0">
   <script type="text/javascript">
    //<![CDATA[
    var disqus_shortname = 'geishastudios';
    (function () {
    var s = document.createElement('script'); s.async = true;
    s.src = 'http://disqus.com/forums/geishastudios/count.js';
    (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
    //]]>
   </script>
  </xsl:if>
  <script type="text/javascript">
   //<![CDATA[
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-2006313-2']);
   _gaq.push(['_trackPageview']);

   (function() {
   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
   //]]>
  </script>
 </xsl:template>
</xsl:stylesheet>

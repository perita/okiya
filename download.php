<?php
require_once('download.local.php');

function downloadFile ($file, $filename)
{
    $time = date('r', filemtime ($file));

    ini_set ('zlib.output_compression', 'off');
    ini_set ('implicit_flushing', 'on');
    header ('Cache-Control: ');
    header ('Pragma: ');
//    header ('Content-Type: ' . getMIMETYPE ($file));
    header ('Content-Type: application/octet-stream');
    header ('Cache-Control: public, must-revalidate, max-age=0');
    header ('Pragma: no-cache');  
    header ('Content-Length: ' . filesize ($file));
    header ("Content-Disposition: attachment; filename=\"$filename\"");
    header ('Content-Transfer-Encoding: binary');
    header ("Last-Modified: $time");
    header ('Connection: close');  

    ob_clean ();
    flush ();
    readfile ($file);
}

function getFileName ($input)
{
    // Remove all unwanted characters from the string.
    return preg_replace ('/[^A-Za-z0-9_\.-]+/', '', $input);
}

function endswith ($string, $postfix)
{
    return (substr($string, -strlen($postfix)) == $postfix);
}

function getMIMEType ($file)
{
    if ( endswith ($file, '.msi') )
    {
        return 'application/x-msdownload';
    }
    else if (endswith ($file, '.apk'))
    {
        return 'application/vnd.android.package-archive';
    }
    return exec ("file -i -b $file");
}

function incrementDownload ($filename)
{
    $link = mysql_connect (DB_HOST, DB_USER, DB_PASS);
    @mysql_select_db (DB_NAME, $link);
    // first, find if the filename already exists.
    $result = mysql_query ("SELECT * FROM downloads WHERE filename = \"$filename\"", $link);
    if ( 0 == mysql_num_rows ($result) )
    {
        mysql_query ("INSERT INTO downloads (filename, downloads) VALUES (\"$filename\", 1)", $link);
    }
    else
    {
        mysql_query ("UPDATE downloads SET downloads = downloads + 1 WHERE filename = \"$filename\"", $link);
    }
    // Some nice stats.
    $ip = $_SERVER['REMOTE_ADDR'];
    $referer = $_SERVER['HTTP_REFERER'];
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    mysql_query ("INSERT INTO download_stats (filename, ip, referer, user_agent) VALUES (\"$filename\", \"$ip\", \"$referer\", \"$user_agent\")", $link);
    mysql_close ($link); 
}

$filename = getFileName ($_GET['file']);
$file = "../files/$filename";
if ( !empty ($filename) && file_exists ($file) )
{
    incrementDownload ($filename);
    downloadFile ($file, $filename);
}
else
{
    header ('HTTP/1.1 404 Not Found');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <title>File Not Found</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  </head>
  <body>
    <h1><span>File Not Found</span></h1>
    <p>The requested file, &#8220;<?=$filename;?>&#8221;, could not be found on the server.  Please go back and try again.</p>
    <p>If the error persists, please contact <a href="mailto:jfita@geishastudios.com">the administrator</a> to inform of this situation.</p>
    <p>I apologize for any inconvenience.</p>
  </body>
</html>
<?php
}
?>

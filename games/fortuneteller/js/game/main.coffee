window.onload = () ->
    main()

main = () ->
    Crafty.init 256, 256
    Crafty.canvas.init()

    # Sprites
    Crafty.sprite 20, 'assets/ship.png',
        ship_sprite: [0, 0]

    Crafty.sprite 256, 'assets/fortuneteller.png',
        teller_sprite: [0, 0]

    Crafty.sprite 18, 'assets/explosion.png',
        explosion_sprite: [0, 0]


    # Components.
    Crafty.c 'Bullet'
        init: () ->
            @requires '2D, DOM, Image, Collision'
            @image 'assets/bullet.png'
            @collision [1, 1], [4, 1], [4, 4], [1, 4]

    Crafty.c 'BulletPlayer'
        init: () ->
            @requires 'Bullet'
            @bind 'EnterFrame', () ->
                @y -= 2
                if @y < -20
                    @destroy()
            @onHit 'TellerMain',
                () ->
                    @x = @x
                () ->
                    @destroy()

    Crafty.c 'BulletEnemy'
        _dirX: 1
        _dirY: 1
        _speed: 2

        init: () ->
            @requires 'Bullet'
            @bind 'EnterFrame', () ->
                @y += @_dirY * @_speed
                @x += @_dirX * @_speed
                if @y < -20 or @y > 276 or @x < -20 or @x > 276
                    @destroy()

        setDest: (x, y) ->
            @_dirX = (x + 5) - @x
            @_dirY = (y + 6) - @y
            magnitude = Math.sqrt @_dirX * @_dirX + @_dirY * @_dirY
            @_dirX /= magnitude
            @_dirY /= magnitude

    Crafty.c 'Explosion'
        init: () ->
            @requires '2D, DOM, SpriteAnimation, explosion_sprite'
            @animate 'explode', 0, 0, 7
            @animate 'explode', 5, 0
            @bind 'EnterFrame', () ->
                if not @isPlaying('explode')
                    @destroy()

        setPos: (x, y) ->
            @x = x - 9
            @y = y - 9

    Crafty.c 'Player'
        _shoot: false
        _shoot_time: 5
        _shoot_timeout: 0
        init: () ->

            @requires 'ShipAnimation, Multiway, Collision'
            @multiway 3, {
                UP_ARROW: -90,
                DOWN_ARROW: 90,
                RIGHT_ARROW: 0,
                LEFT_ARROW: 180}
            @collision [8, 8], [12, 8], [12, 12], [8, 12]
            @onHit 'BulletEnemy', () ->
                bullet = Crafty.e 'Explosion'
                bullet.setPos @x + 10, @y + 10
                @destroy()
            @bind 'EnterFrame', () ->
                @x = Math.max(Math.min(@x, 236), 0)
                @y = Math.max(Math.min(@y, 236), 0)
                if @_shoot
                    if @_shoot_timeout <= 0
                        @_shoot_timeout = @_shoot_time
                        @shoot()
                    else
                        @_shoot_timeout--

            @bind 'KeyDown', (e) ->
                if e.key is Crafty.keys['SPACE']
                    @_shoot = true
                    @_shoot_timeout = 0
            @bind 'KeyUp', (e) ->
                if e.key is Crafty.keys['SPACE']
                    @_shoot = false

        shoot: () ->
            bullet = Crafty.e 'BulletPlayer'
            bullet.attr {x: @x + 7, y: @y - 7}

    Crafty.c 'ShipAnimation',
        init: () ->
            @requires '2D, DOM, SpriteAnimation, ship_sprite'
            @animate 'fly', 0, 0, 1
            @animate 'fly', 5, -1

    Crafty.c 'ShipIntro',
        _end: false
        init: () ->
            @requires 'ShipAnimation, Time'
            @bind 'EnterFrame', () ->
                if @y > 192
                    @y -= 0.5
                else
                    if not @_end
                        Crafty('TellerIntro').each ()->
                            @fadeIn()
                        @timeout () ->
                            Crafty.scene 'main'
                        , 2000
                        @_end = true
                    @y = 192

    Crafty.c 'Teller'
        init: () ->
            @requires '2D, DOM, Image'
            @image 'assets/fortuneteller.png'

    Crafty.c 'TellerIntro',
        init: () ->
            @requires 'Teller, Tween'
            @attr {alpha: 0.0}

        fadeIn: () ->
            @tween {alpha: 1.0}, 100

    Crafty.c 'TellerMain',
        _shoot_timeout: 20
        _shoot_time: 10
        _ship: null
        _hit_time: 0
        _life: 1000
        init: () ->
            @requires '2D, DOM, SpriteAnimation, teller_sprite, Collision'
            @animate 'regular', 0, 0, 0
            @animate 'hit', 1, 0, 1
            @collision [113, 91], [152, 91], [138, 101], [127, 102]
            @onHit 'BulletPlayer',
                () ->
                    if @_life > 0
                        @animate 'hit', 1, -1
                        @_life--
                        console.log @_life
                        if @_life is 0
                            Crafty('BulletEnemy').each () ->
                                @destroy()
                            @animate 'regular', 1, -1
                            @explode(100)
                () ->
                    @animate 'regular', 1, -1


            @bind 'EnterFrame', () ->
                if @_life > 0
                    if @_shoot_timeout < 0
                        @_shoot_timeout = @_shoot_time
                        @shoot()
                    else
                        @_shoot_timeout--
                    if @_hit_time == 0
                        @animate 'regular', 1, -1
                        @_hit_time = -1
                    else if @_hit_time > 0
                        @_hit_time--

        shoot: () ->
            left_bullet = Crafty.e 'BulletEnemy'
            left_bullet.attr {x: 111, y: 54}
            left_bullet.setDest @_ship.x, @_ship.y
            right_bullet = Crafty.e 'BulletEnemy'
            right_bullet.attr {x: 141, y: 48}
            right_bullet.setDest @_ship.x, @_ship.y

        explode: (count) ->
            if count > 0
                x = Math.floor(Math.random() * 256 + 1)
                y = Math.floor(Math.random() * 256 + 1)
                explosion = Crafty.e 'Explosion'
                explosion.setPos x, y
                @timeout () =>
                    @explode(count - 1)
                , 10
            else
                @timeout () ->
                    Crafty.scene 'end'
                , 1000

        setShip: (ship) ->
            @_ship = ship

    # Scenes
    Crafty.scene 'loading', () ->
        Crafty.background '#000'
        Crafty.load [
            'assets/ship.png', 'assets/fortuneteller.png', 'assets/bullet.png',
            'assets/explosion.png'],
            () ->
                Crafty.scene 'wait'
        Crafty.e('2D, DOM, Text')
            .attr(w: 256, h: 20, x: 0, y: 118)
            .text('Loading')
            .css(
                'text-align': 'center',
                'color': '#fff')

    Crafty.scene 'wait', () ->
        Crafty.background '#000'
        Crafty.e('2D, DOM, Text')
            .attr(w: 256, h: 20, x: 0, y: 118)
            .text('Click to Play')
            .css(
                'text-align': 'center',
                'color': '#fff')
        Crafty.e('2D, DOM, Mouse')
            .attr(w: 256, h: 256, x: 0, y: 0)
            .bind('Click', () ->
                Crafty.scene 'intro'
            )

    Crafty.scene 'end', () ->
        Crafty.background '#000'
        Crafty.e('2D, DOM, Text')
            .attr(w: 256, h: 20, x: 0, y: 118)
            .text('Congrats!')
            .css(
                'text-align': 'center',
                'color': '#fff')

    Crafty.scene 'intro', () ->
        teller = Crafty.e 'TellerIntro'
        ship = Crafty.e 'ShipIntro'
        ship.attr x: 128, y: 256

    Crafty.scene 'main', () ->
        teller = Crafty.e 'TellerMain'
        ship = Crafty.e 'Player'
        ship.attr x: 128, y: 192
        teller.setShip ship

    # Load the first scene
    Crafty.scene 'loading'

.PHONY: site clean literate

SITE_URL=http://www.geishastudios.com/
ASCIIDOC_CONF = geishastudios.conf

ifdef VERBOSE
QUIET =
else
QUIET = @
endif

WIKI_FILES = $(patsubst ./%, %, $(shell find . -type f -name '*.wiki'))
HTML_FILES = $(patsubst %.wiki, %.html, $(WIKI_FILES))
XSL_STYLESHEET = geishastudios.xsl

ifeq ($(wildcard literate), literate)
SITEMAPS = sitemap.xml literate/sitemap.xml
else
SITEMAPS = sitemap.xml
endif

site: $(HTML_FILES) literate sitemap.xml sitemapindex.xml


literate:
ifeq ($(wildcard literate), literate)
	$(MAKE) SITE_URL=$(SITE_URL) XSL_STYLESHEET=`pwd`/$(XSL_STYLESHEET) ASCIIDOC_OPTIONS="-f `pwd`/${ASCIIDOC_CONF}" -C literate html
endif

%.html: %.xml $(XSL_STYLESHEET)
	$(QUIET)xsltproc --nonet --stringparam site.topdir "$$(dirname $< | sed -e 's#[^/.][^/]*#..#g')" --output $@ $(XSL_STYLESHEET) $<
	$(QUIET)rm -f $<

%.xml: %.wiki $(ASCIIDOC_CONF)
	@echo Processing $<...
	$(QUIET)asciidoc --conf-file=$(ASCIIDOC_CONF) --backend=docbook --attribute=toc! --attribute=numbered! --doctype=article --out-file=$@ $<

index.xml: index.wiki games/index.wiki $(ASCIIDOC_CONF)
	@echo Processing $<...
	$(QUIET)asciidoc --conf-file=$(ASCIIDOC_CONF) --backend=docbook --attribute=toc! --attribute=numbered! --doctype=article --out-file=$@ $<

sitemap.xml: $(HTML_FILES)
	echo '<?xml version="1.0" encoding="UTF-8"?>' > $@
	echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' >> $@
	for page in $(HTML_FILES); do \
		echo ' <url>' >> $@ ; \
		echo "  <loc>$(SITE_URL)$$page</loc>" >> $@ ; \
		(echo -n '  <lastmod>'; date -ur $$page +%FT%T%:z | tr -d '\n' ; echo '</lastmod>') >> $@ ; \
		echo '  <changefreq>monthly</changefreq>' >> $@ ; \
		echo '  <priority>0.8</priority>' >> $@ ; \
		echo ' </url>' >> $@ ; \
	done
	echo '</urlset>' >> $@

sitemapindex.xml: $(SITEMAPS)
	echo '<?xml version="1.0" encoding="UTF-8"?>' > $@
	echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' >> $@
	for sitemap in $(SITEMAPS); do \
		echo ' <sitemap>' >> $@ ; \
		echo "  <loc>$(SITE_URL)$$sitemap</loc>" >> $@; \
		(echo -n '  <lastmod>'; date -ur $$sitemap +%FT%T%:z | tr -d '\n' ; echo '</lastmod>') >> $@ ; \
		echo ' </sitemap>' >> $@ ; \
	done
	echo '</sitemapindex>' >> $@


clean:
	rm -f $(HTML_FILES) sitemap.xml sitemapindex.xml
